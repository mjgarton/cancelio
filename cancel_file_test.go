package cancelio

import (
	"io"
	"os"
	"testing"
	"time"
)

func TestHandlesFile(t *testing.T) {
	r, w, err := os.Pipe()
	if err != nil {
		t.Fatal(err)
	}

	doTest(r, t)
	w.Close()
}

func doTest(file *os.File, t *testing.T) {
	cr, err := NewCancelFile(file)
	if err != nil {
		t.Fatal(err)
	}

	readExited := make(chan struct{})

	// let's start 16 readers
	for i := 0; i < 16; i++ {
		go func() {
			defer func() { readExited <- struct{}{} }()
			buf := make([]byte, 1024)
			_, err = cr.Read(buf)
			if err != nil && err != ReadCancelled {
				t.Fatal(err)
				return
			}

		}()
	}

	time.Sleep(50 * time.Millisecond)

	// should be blocking at this point
	select {
	case <-readExited:
		t.Fatal("read should have blocked")
	default:
	}

	cr.Close()

	// reads should all have exited now
	for i := 0; i < 16; i++ {
		select {
		case <-readExited:
		case <-time.After(100 * time.Millisecond):
			t.Fatal("read should have exited")
		}
	}

	// should be no more exited reads
	select {
	case <-readExited:
		t.Fatal("too many 'exited' reads")
	default:
	}
}

func TestIO(t *testing.T) {
	r, w, err := os.Pipe()
	if err != nil {
		t.Fatal(err)
	}

	defer w.Close()

	cr, err := NewCancelFile(r)
	if err != nil {
		t.Fatal(err)
	}

	data := make(chan []byte, 128)

	// start 4 readers, each to read 4 times
	for i := 0; i < 4; i++ {
		go func(rn int) {
			for j := 0; j < 4; j++ {
				buf := make([]byte, 1024)
				i, err = cr.Read(buf)
				if err != nil {
					// even EOF is an error for us
					return
				}
				t.Logf("reader number %d did a read. count is %d\n", rn, j)
				data <- buf[0:i]
			}
		}(i)
	}

	// write some data to the other end of the underlying reader
	for i := 0; i < 16; i++ {
		time.Sleep(2 * time.Millisecond)
		w.Write([]byte{byte(i)})
	}

	// check reads
	for i := 0; i < 16; i++ {
		d := <-data
		t.Logf("%v\n", i)
		if len(d) != 1 || int(d[0]) != i {
			t.Error("unexpected data")
		}
	}

	if len(data) != 0 {
		t.Error("too much data")
	}

}

func BenchmarkRawPipe(b *testing.B) {
	r, w, err := os.Pipe()
	if err != nil {
		b.Fatal(err)
	}

	benchmarkPipe(b, r, w)
}

func BenchmarkCancelIOPipe(b *testing.B) {
	r, w, err := os.Pipe()
	if err != nil {
		b.Fatal(err)
	}

	cr, err := NewCancelFile(r)
	if err != nil {
		b.Fatal(err)
	}
	benchmarkPipe(b, cr, w)
}

func benchmarkPipe(b *testing.B, r io.ReadCloser, w io.WriteCloser) {
	closeWrite := make(chan struct{})

	go func() {
		data := make([]byte, 1024)
		for {
			select {
			case <-closeWrite:
				w.Close()
				return
			default:
				if _, err := w.Write(data); err != nil {
					//	b.Error(err)
					return
				}
			}
		}
	}()

	buf := make([]byte, 1024)
	for i := 0; i < b.N; i++ {
		_, err := r.Read(buf)
		if err != nil {
			b.Fatal(err)
		}

	}
	close(closeWrite)
	r.Close()
}
