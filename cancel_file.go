// Copyright (C) 2015 Martin Garton <garton@gmail.com>
package cancelio

import (
	"errors"
	"log"
	"os"
	"syscall"
)

type CancelFile struct {
	*os.File
	closed           chan struct{}
	readReady        chan struct{}
	cancelr, cancelw *os.File
}

func NewCancelFile(file *os.File) (*CancelFile, error) {

	cancelr, cancelw, err := os.Pipe()
	if err != nil {
		return nil, err
	}

	var cf = CancelFile{
		file,
		make(chan struct{}),
		make(chan struct{}),
		cancelr,
		cancelw,
	}

	go cf.loop(file.Fd())

	return &cf, nil
}

func (cr *CancelFile) loop(datafd uintptr) {
	defer close(cr.closed)
	defer cr.cancelr.Close()
	closepipefd := cr.cancelr.Fd()
	for {
		maxfd := datafd
		if maxfd < closepipefd {
			maxfd = closepipefd
		}
		var fdset syscall.FdSet
		fd_set(datafd, &fdset)
		fd_set(closepipefd, &fdset)

		ret, err := syscall.Select(int(maxfd)+1, &fdset, nil, nil, nil)
		if err != nil || ret < 0 {
			if err == syscall.EBADF {
				return
			}
			log.Fatalf("select failed. Not sure what this means. exiting. %T", err)
		}
		if fd_isset(datafd, &fdset) {
			// signal that a read is ready
			cr.readReady <- struct{}{}
		} else if fd_isset(closepipefd, &fdset) {
			// we have been asked to quit
			return
		} else {
			panic("probable bug in select() processing. exiting")
		}
	}
}

func (cr *CancelFile) Read(buf []byte) (int, error) {
	select {
	case <-cr.closed:
		return 0, ReadCancelled
	case <-cr.readReady:
		return cr.File.Read(buf)
	}
}

func (cr *CancelFile) Close() error {
	err := cr.File.Close()
	cr.cancelw.Close()
	return err
}

func fd_set(fd uintptr, p *syscall.FdSet) {
	p.Bits[fd/32] |= (1 << uint32(fd%32))
}

func fd_isset(fd uintptr, p *syscall.FdSet) bool {
	return (p.Bits[fd/32] & (1 << uint32(fd%32))) != 0
}

var ReadCancelled = errors.New("read cancelled")
