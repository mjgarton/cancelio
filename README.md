## cancelio for golang

This is an attempt to enable graceful exit of Read() operations upon Close()
of os.File.

An example of where this might be useful is when dealing with /dev/net/tun or
similar where there is no "other end" to close.

